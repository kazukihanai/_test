<?php

class Test
{
  public $result;
  public $option;

  function __construct()
  {
    /***
     * 使用するクラスをglobal宣言
     */
    global $smObj;

    /***
     * ファイルのinclude
     */
    include("/vagrant/www/includes/news/index.php");

    /***
     * テストする変数を代入
     */
    $this->result = $result;
    $this->option = $DAO->options;
  }
}

class indexTest extends \PHPUnit_Framework_TestCase
{
  private $target;

  public function setUp()
  {
    $this->target = new Test();
  }

  /***
   * $resultをテスト
   */
  public function testResult()
  {
    //DBからデータを取得
    $DAO = new NewsDAO;
    $result = $DAO->findAll();

    //DBとのデータと比較
    $this->assertEquals($result, $this->target->result);
  }
}
