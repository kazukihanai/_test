<?php

class NewsDAOTest extends \PHPUnit_Framework_TestCase
{
  private $target = null;
  private $sample = array();

  public function setUp()
  {
    //テスト用クラス
    $this->target = new NewsDAO;

    //検証用のサンプルデータ
    $this->sample = array(
      "post_date" => date("Y-m-d"),
      "title"     => "plusstyle アイデア募集のお知らせ",
      "content"   => "吾輩わがはいは猫である。名前はまだ無い。\nどこで生れたかとんと見当けんとうがつかぬ",
      "status"    => 1
    );

  }

  /***
   * デフォルトの配列テスト
   */
  public function testGetDefaultValues()
  {
    //デフォルトの配列を宣言
    $params = array(
      "post_date" => date("Y-m-d"),
      "pickup"    => 0,
      "status"    => 0
    );

    //デフォルトの配列が一致するか
    $this->assertEquals($params, $this->target->getDefaultValues());
  }

  /***
   * 入力文字チェックテスト
   */
  public function testCheck()
  {
    // // post_dateに関するチェックテスト
    // $check1 = $this->sample;
    // $check1['post_date'] = "2016-10-16";
    // $result1 = $this->target->check($check1);
    // $this->assertTrue(empty($result1['error']));
    //
    // $check1['post_date'] = "     2016-10-16 ";
    // $result1 = $this->target->check($check1);
    // $this->assertTrue(empty($result1['error']));
    //
    // $check1['post_date'] = "2016-10-16 ";
    // $result1 = $this->target->check($check1);
    // $this->assertTrue(empty($result1['error']));
    //
    // $check1['post_date'] = "2016-10-16 ";
    // $result1 = $this->target->check($check1);
    // $this->assertTrue(empty($result1['error']));
    //
    //
    // // titleに関するチェックテスト
    // $check2 = $this->sample;
    // $check2['title'] = "testテスト🙌";
    // $result2 = $this->target->check($check2);
    // $this->assertTrue(empty($result2['error']));
    //
    // $check2['title'] = "     テスト　　　　　";
    // $result2 = $this->target->check($check2);
    // $this->assertTrue(empty($result2['error']));
    //
    // $check2['title'] = "";
    // $result2 = $this->target->check($check2);
    // $this->assert(empty($result2['error']));
    //
    // $check2['title'] = "";
    // $result2 = $this->target->check($check1);
    // $this->assertTrue(empty($result2['error']));
    //
    // // contentに関するチェックテスト
    // $check3 = $this->sample;
    // $check3['content'] = "";
    // $result3 = $this->target->check($check3);
    // $this->assertTrue(empty($result3['error']));
    //
    // $check3['content'] = "";
    // $result3 = $this->target->check($check3);
    // $this->assertTrue(empty($result3['error']));
    //
    // $check3['content'] = "";
    // $result3 = $this->target->check($check3);
    // $this->assertTrue(empty($result3['error']));
    //
    // $check3['content'] = "";
    // $result3 = $this->target->check($check3);
    // $this->assertTrue(empty($result3['error']));
    //
    // // statusに関するチェックテスト
    // $check4 = $this->sample;
    // $check4['status'] = "";
    // $result4 = $this->target->check($check4);
    // $this->assertTrue(empty($result4['error']));
    //
    // $check4['status'] = "";
    // $result4 = $this->target->check($check4);
    // $this->assertTrue(empty($result4['error']));
    //
    // $check4['status'] = "";
    // $result4 = $this->target->check($check4);
    // $this->assertTrue(empty($result4['error']));
    //
    // $check4['status'] = "";
    // $result4 = $this->target->check($check4);
    // $this->assertTrue(empty($result4['error']));
    //
  }

  /***
   * 配列の不要な部分を削除テスト
   */
  public function testPrepare()
  {
    //余計な値を持った配列の宣言
    $result = $this->sample;
    $result['submit'] = "test";
    $result['file'] = "/vagrant/www";
    $result['csrf_token'] = "dfj390fj2klf3atjukh9afzf50jf";

    //冗長な部分が削られているかどうか
    $this->assertEquals($this->sample, $this->target->prepare($result));
  }

  /***
   * DBから個別(id)で行を取得テスト
   */
  public function testFindById()
  {
    // id = 1の要素を取得
    $id = 1;
    $result = $this->target->findById($id);

    // id = 1ならOK
    $this->assertEquals($id,$result['id']);
  }

  /***
   * DBから一覧取得テスト
   */
  public function testFindAll()
  {
    //データの取得
    $result = $this->target->findAll();

    //要素数が同じならOK
    $this->assertEquals(1,count($result['data']));
  }

  /***
   * DBからピックアップ一覧を取得テスト
   */
  public function testFindAllbyPickupAll()
  {
    //データ受け取り
    $result = $this->target->findAllbyPickupAll();

    // id = 1だったらOK
    $this->assertEquals(1,$result[0]['id']);
  }

  /***
   * DBにデータを追加テスト
   */
  public function testInsert()
  {
    //DBデータ挿入
    $this->target->insert($this->sample);

    //データ受け取り
    $result1 = $this->target->findAll();

    //データが挿入されているか
    $this->assertEquals($this->sample['content'], $result1['data'][0]['content']);
  }

  /***
   * DBのデータを更新テスト
   */
  public function testUpdate()
  {
    //比較用データの作成
    $result1 = $this->target->findAll();

    //更新用データを作成
    $sample_update = $this->sample;
    $sample_update['id'] = $result1['data'][0]['id'];
    $sample_update['title'] = "plusstyle 更新のお知らせ";

    //データ更新
    $this->target->update($sample_update);

    //データの取得
    $result1 = $this->target->findAll();

    $this->assertEquals($sample_update['title'],$result1['data'][0]['title']);
  }

  /***
   * DBの追加したデータを削除テスト
   */
  public function testDelete()
  {
    //idを取得
    $result1 = $this->target->findAll();

    //DBのデータを削除
    $this->target->delete($result1[data][0]['id']);

    //データが帰ってこなければ成功
    $result2 = $this->target->findAll();

    $this->assertEquals(1,count($result2['data']));
  }


}
