<?php

class UsersDAOTest extends \PHPUnit_Framework_TestCase
{
  private $test = null;

  public function setUp()
  {

    $this->test = new UsersDAO;

  }

  public function testCheck()
  {
    $pa = array(
      "nickname" => "やまぴー",
      "first_name" => "太郎",
      "last_name" => "山田",
      "first_name_kana" => "たろう",
      "last_name_kana" => "やまだ",
      "email" => "masaaki.eda@fourier.jp",
      "password" => "123456",
      "img_url" => "http://www.yahoo.co.jp",
      "img_url_sns" => "http://www.yahoo.co.jp",
      "hp" => "http://www.yahoo.co.jp",
      "country" => "日本",
      "zipcode" => "2210865",
      "pref" => "神奈川県",
      "address1" => "横浜市神奈川区片倉",
      "address2" => "5-15-13",
      "address3" => "サンフラワーB 302号室",
      "birthday" => "1984/4/22",
      "sex" => "男",
      "tel" => "08012345678",
      "company" => "株式会社フーリエ",
      "message" => "吾輩わがはいは猫である。名前はまだ無い。\nどこで生れたかとんと見当けんとうがつかぬ。",
      "kind" => "0",
    );
    $pa['submit'] = array("new" => "new");

    $type = null;
    $result = array();
    $result = $this->test->check($pa,$type);
    $this->assertTrue(true);

  }
}
