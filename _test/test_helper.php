<?php

session_start();

/**
 * 外部読み込みファイル群
 * -------------------------------------------------------------------------- */
include ('/vagrant/www/web-config.php');
include (__ABSPATH__.'vendor/autoload.php');
include (LIBPATH.'common/Pager_Wrapper.php');

/**
 * サニタイズ／エスケープ処理
 * ------------------------------------------------------------------ */
$_SERVER["REQUEST_URI"]       = htmlspecialchars($_SERVER["REQUEST_URI"], ENT_QUOTES, 'UTF-8');
$_SERVER["HTTP_REFERER"]      = htmlspecialchars($_SERVER["HTTP_REFERER"], ENT_QUOTES, 'UTF-8');
$_SERVER["REMOTE_ADDR"]       = htmlspecialchars($_SERVER["REMOTE_ADDR"], ENT_QUOTES, 'UTF-8');
$_SERVER["SERVER_NAME"]       = htmlspecialchars($_SERVER["SERVER_NAME"], ENT_QUOTES, 'UTF-8');
$_SERVER["HTTP_HOST"]         = htmlspecialchars($_SERVER["HTTP_HOST"], ENT_QUOTES, 'UTF-8');
$_SERVER["HTTP_X_CSRF_TOKEN"] = htmlspecialchars($_SERVER["HTTP_X_CSRF_TOKEN"], ENT_QUOTES, 'UTF-8');
$_SERVER["HTTP_USER_AGENT"]   = htmlspecialchars($_SERVER["HTTP_USER_AGENT"], ENT_QUOTES, 'UTF-8');

/**
 * アクセス制限
 * ------------------------------------------------------------------ */
if(!in_array($_SERVER['SERVER_PORT'], explode(",",ALLOW_PORTS))){
  header("HTTP/1.0 404 Not Found");
  echo 'access deny.';
  exit;
}

/**
 * PAYMENT URL処理
 * ------------------------------------------------------------------ */
//$uri = (preg_match('/payment/',$_SERVER['HTTP_HOST'])) ? _URI_PAY_ : _URI_;
if(preg_match('/payment/',$_SERVER['HTTP_HOST'])) {
  $uri = _URI_PAY_;
}else{
  $uri = _URI_;
}

/**
 * クラス外部ファイル自動読み込み関数
 * -------------------------------------------------------------------------- */
spl_autoload_register(function ($className) {
  $dirs = array('', 'common/');
  foreach ($dirs as $dir) {
    $path =  LIBPATH . $dir . $className . '.class.php';
    if (is_readable($path)) {
      include_once $path;
      return;
    }
  }
});

/**
 * DB接続準備
 * -------------------------------------------------------------------------- */
//MySQL
$mdb2 =& MDB2::factory($db_dsn);
$mdb2->setFetchMode(MDB2_FETCHMODE_ASSOC);
$mdb2->loadModule('Extended'); // autoExecute()の有効化


/**
 * initial
 * ------------------------------------------------------------------ */
$meta          = array();
$pa            = array();
$msg           = array();
$is_mdb        = 0;

/**
 * オブジェクト生成
 * ------------------------------------------------------------------ */
global $FRFW, $PSC, $smObj, $uaObj, $selObj, $elObj, $icObj;
$FRFW       = new FourierFramework;  // フーリエフレームワーク
$PSC        = new PlusStyleCORE;     // プラススタイル・共通クラス群
$smObj      = new Smarty;
$uaObj      = new UAChecker();
$selObj     = new UriSelector($uri ,$uaObj->ua['device']); // pc or sp でtemplatesディレクトリ分け
$elObj      = new ErrorLog;
$icObj      = new InputCheck;
$authObj    = new AuthEx($authParams);
$paymentObj = new PaymentServiceForLink;

/**
 * Smarty
 * ------------------------------------------------------------------ */
$smObj->template_dir = dirname(__FILE__).'/templates/';
$smObj->compile_dir  = dirname(__FILE__).'/templates_c/';
//$smObj->debugging    = ($_SERVER['REMOTE_ADDR'] == '192.168.33.1') ? true : false;

// /**
//  * エスケープ処理
//  * ------------------------------------------------------------------ */
// if ($_SERVER["REQUEST_METHOD"] == "POST") {$pa = $FRFW->htmlspecialchars_encode_array($_POST);}
// else {$pa = $FRFW->htmlspecialchars_encode_array($_GET);}
//
// /**
//  * オブジェクト実行
//  * ------------------------------------------------------------------ */
// $authflg = $authObj->start($pa);
// $auth    = $_SESSION['auth'];
// //ユーザ限定ページ確認
// $authObj->isLimitedPage($authflg);
//
// /**
//  * ページコントローラ
//  * ------------------------------------------------------------------ */
// // permalink check
// // .htmlはスルー
// if (!strstr($_SERVER['REQUEST_URI'], '.html')) {
//   //permalink対象
//   $rewrite_list = array('planning', 'funding', 'shopping');
//   $regex = "/\/(planning|funding|shopping)\/([a-zA-Z0-9_-]+)/";
//   //permalink対象チェック
//   if (preg_match($regex, $_SERVER['REQUEST_URI'])) {
//     //実態ファイルが存在するかチェック、存在する場合はスキップ（書き換え処理を行わない）
//     if (!file_exists($smObj->template_dir[0].$selObj->tplFile)) {
//       foreach ($rewrite_list as $v) {
//         // get parmalink
//         preg_match("/\/{$v}\/([a-zA-Z0-9_-]+)/", $_SERVER['REQUEST_URI'], $matches);
//         $permalink = $matches[1];
//         // overwrite tpl / ctl file.
//         if (!empty($permalink)) {
//           $selObj->tplFile = $uaObj->ua['device'] . "/{$v}/item.tpl";
//           $selObj->ctlFile = "{$v}/item.php";
//           break;
//         }
//       }
//     }
//   }
// }
//
// if(isset($selObj->ctlFile) && $selObj->ctlFile!=""){
//   $incfile = 'includes/'.$selObj->ctlFile;
// 	if(file_exists($incfile))	include ($incfile);
// }
//
// if($_SERVER["REMOTE_ADDR"] == "122.249.204.181") {
//   $sses = $_SERVER["SERVER_ADDR"].':'.$_SERVER["REMOTE_HOST"].':'.$_SERVER["REMOTE_ADDR"];
// }
//
//
// /**
//  * オブジェクト実行(ページコントローラ呼び出し後)
//  * ------------------------------------------------------------------ */
// //カート情報取得
// $cnt_cart = $paymentObj->getCntCartbyUsersId($auth['id']);
